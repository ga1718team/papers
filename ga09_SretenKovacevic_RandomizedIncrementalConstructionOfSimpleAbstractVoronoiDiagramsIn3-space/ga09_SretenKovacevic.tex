\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{colortbl}
\usepackage{amssymb}
\usepackage{amsmath}

\graphicspath{ {images/} }
\usepackage{listings}
\usepackage{xcolor}

\usepackage{dirtytalk}

\usepackage[english,serbian]{babel}

\usepackage[unicode]{hyperref}
\hypersetup{colorlinks, citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}

\begin{document}

\title{Nasumi\v{c}na inkrementalna konstrukcija jednostavnog apstraktnog Voronoj dijagrama u prostoru}

\author{Sreten Kova\v{c}evi\'{c}\\ mi13097@matf.bg.ac.rs}

\maketitle

\section{Uvod}

Ovaj rad predstavlja kratak sa\v{z}etak i prilago\dj{}avanje na srpski jezik rada \textit{Randomized incremental construction of simple abstract Voronoi diagrams in 3-space}\cite{randincconstr3}. Rad je napisan 1995. godine i objavljen je u \v{c}asopisu \textit{Computational  Geometry: Theory and Applications}. Najve\'{c}i deo rada napisan je dok je autor boravio na univerzitetu u Hagenu na odeljenju za primenjenu informatiku, te je rad jednim delom finansiran iz sredstava tog univerziteta i Nema\v{c}ke vlade.

Zbog ograni\v{c}enja u obimu rada, u nastavku \'{c}e biti prikazana samo gruba skica obra\dj{}enog rada. Izostavljeni su svi dokazi, kao i odre\dj{}eni broj definicija i lema koji su u datim dokazima kori\v{s}\'{c}eni, ali koji nisu neophodni za razumevanje same ideje koju ovaj rad poku\v{s}ava da predstavi.

\section{Autor}

Autor obra\dj{}enog rada je \textit{Ngoc-Minh Lê}, koji je \v{c}lan odeljenja za sisteme i mre\v{z}e, kao i odeljenja za informacione tehnologije na tehnolo\v{s}kom univerzitetu u Ho Chi Minhu (ranije poznat kao Saigon) u Vijetnamu. Studije zavr\v{s}io na univerzitetu u Hagenu, Nema\v{c}ka.

Izdao je brojne radove iz oblasti, kako teorijeske, tako i primenjene informatike. Neki od radova su: \textit{Abstract Voronoi diagram in 3-space}, \textit{On Voronoi diagrams in the Lp-metric in $\mathbb{R}^0$}, \textit{On voronoi diagrams in the Lp-metric in higher dimensions}.

\section{Opis problema}

Cilj rada je konstruisati algoritam koji \'{c}e odrediti jednostavan apstraktni Voronoj dijagram u prostoru sa o\v{c}ekivanom vremenskom i prostornom slo\v{z}eno\v{s}\'{c}u $O(n^2)$, gde je $n$ broj sedi\v{s}ta. Ideja je da se koristi tehnika nasumi\v{c}ne inkrementalne konstrukcije (tehnika konstruisanja Klarksona(\textit{Clarkson}) i \v{S}ora(\textit{Shor}) iz 1989. godine).

Planarni grafovi nad razli\v{c}itim metrikama, mogu se posmatrati kao specijalizacija jednog tipa dijagrama, koji se naziva apstraktni Voronoj dijagrami. Klejn(\textit{Klein}) je aksiomatski definisao ovakav pogled, posmatraju\'{c}i apstraktne Voronoj dijagrame kroz presecaju\'{c}e krive. Za svaka dva sedi\v{s}ta \textit{p} i \textit{q} postoji beskona\v{c}na kriva koja deli ravan na dve oblasti. Voronoj oblast sedi\v{s}ta \textit{p} odre\dj{}ena je krivama u odnosu na druga sedi\v{s}ta. On je razvio(u saradnji sa drugim nau\v{c}nicima) nasumi\v{c}ni inkrementalni algoritam koji odre\dj{}uje apstraktni Voronoj dijagram u ravni u $O(n$log$n)$. Taj algoritam je kori\v{s}\'{c}en kao osnova za onaj uveden u obra\dj{}enom radu.

\section{Rezultati}

Za po\v{c}etak, potrebno je detaljnije analizirati sam dijagram. Ozna\v{c}imo sa $B(p, q)$ prese\v{c}nu povr\v{s} koja deli prostor na region $p$ i region $q$. Kako koristimo Voronoj dijagram pod Euklidskim rastojanjem kao model za aksiome, mo\v{z}emo pretpostaviti da za skup od 5 ta\v{c}aka $p$, $q$, $r$, $s$ i $t$ va\v{z}e slede\v{c}a tvr\dj{}enja:
\begin{enumerate}
	\item Skup $B(p, q) \cap B(q, r)$ je beskona\v{c}na kriva;
	\item Skup $B(p, q) \cap B(q, r) \cap B(r, s)$ je ta\v{c}ka;
	\item Skup $B(p, q) \cap B(q, r) \cap B(r, s) \cap B(s, t)$ je prazan skup.
\end{enumerate}

Tako\dj{}e, prese\v{c}ne povr\v{s}i se seku popre\v{c}no. Dijagram dobijen ovakvim presecima se naziva apstraktni Voronoj dijagram u prostoru.

Neka je $n \in \mathbb{N}$ i neka je $S = \{1,...,n-1\}$. Defini\v{s}emo dominantni sistem nad S kao familiju $\mathcal{D}=\{D(p, q)|p, q \in S \land p\neq q\}$ podskupova prostora za koju va\v{z}i:
\begin{itemize}
	\item $D(p, q)$ je neprazan otvoren podskup prostora;
	\item $D(p, q) \cap D(q, p) = \emptyset$ i $granica(D(p, q)) = granica(D(q, p))$;
	\item $B(p, q) := granica(D(p, q))$ je homeomorfna na ravan u prostoru.
\end{itemize}

Elementi skupa $S$ su sedi\v{s}ta, povr\v{s} $B(p, q)$ je presek $p$ i $q$, a $D(p, q)$ je region dominacije $p$ nad $q$.

\textbf{Definicija 1:} Ako postavimo
\[
	R(p, q) = 
	\begin{cases}
		D(p, q) \cup B(p, q),& \text{ako } p < q \\
		D(p, q),& \text{ako } p > q 
	\end{cases}
\]
i defini\v{s}emo \textit{Pro\v{s}ireni Voronoj region} sedi\v{s}ta \textit{p} u odnosu na \textit{S} kao\\
$EVR(p, S) = \bigcap\limits_{\substack{q \in S\\q \neq p}}R(p, q)$, onda je \textit{Voronoj region} sedi\v{s}ta \textit{p} u odnosu na \textit{S} definisan kao $VR(p, S) = int(EVR(p, S))$, gde je $int$ unutra\v{s}njost figure. Tako\dj{}e, \textit{Apstraktni Voronoj dijagram} u odnosu na \textit{S} defini\v{s}e se sa
\[
V(S) = \bigcup\limits_{p \in S} granica(VR(p, S)).
\]

Tako\dj{}e, potrebno je dodati skupu \textit{S} ta\v{c}ku $\infty$, kako bi se na odgovaraju\'{c}i na\v{c}in dobila ograni\v{c}avaju\'{c}a sfera za dijagram koja garantuje da \'{c}e svaki region biti ograni\v{c}en.

Posmatrajmo $R \subset S$ sa $\infty \in R$, gde je $|R| \geq 4$ i $u \in S \backslash R$. Potrebno je videti \v{s}ta se de\v{a}va prilikom dodavanja sedi\v{s}ta u apstraktni Voronoj dijagram.

\textbf{Lema 1:} Neka je $\mathcal{U}:=VR(u, R \cup \{u\})$. Ako je $\mathcal{U} \neq \emptyset$ onda je $V(R) \cap zatvorenje(\mathcal{U})$ neprazan povezan skup koji se se\v{c}e sa granicom od $\mathcal{U}$.

\textbf{Lema 2:} Neka je $\mathcal{U}:=VR(u, R \cup \{u\})$ i neka je $f$ lice $V(R)$. Pretpostavimo da je ono jednostavno i povezano. Ako je $f \cap zatvorenje(\mathcal{U}) \neq \emptyset$ onda je:
\begin{itemize}
	\item Skup $f \backslash zatvorenje(\mathcal{U})$ je jednostavno povezan (ako je neprazan). Presek $zatvorenje(f)$ i $zatvorenje(\mathcal{U})$ je homeomorfan do na zatvoreni disk.
	\item Presek $granica(f)$ i $zatvorenje(\mathcal{U}$ je homeomorfan do na zatvoren interval ili jednostavnu zatvorenu krivu.
\end{itemize}

Skelet Voronoj dijagrama ozna\v{c}avamo sa
\[
Skel(R) := (\bigcup\limits_{e \in Ivica(R)}e) \cup (\bigcup\limits_{x \in \textit{\v{C}vor}(R)} x).
\]

\textbf{Lema 3:} Ako je $\mathcal{U} \neq \emptyset$ onda:
\begin{itemize}
	\item Presek $Skel(R)$ i $zatvorenje(\mathcal{U})$ je neprazan povezan skup.
	\item Presek $Skel(R)$ i $granica(\mathcal{U})$ je neprazan povezan skup i nije jedna ta\v{c}ka.
\end{itemize}

\textbf{Lema 4:} Neka je $e \in Edge(R)$. Ako $e \cap zatvorenje(\mathcal{U})$ onda je $e \cap zatvorenje(\mathcal{U})$ jedna komponenta, kao i $e \backslash zatvorenje(\mathcal{U})$ (ali ona mo\v{z}e biti i prazna).

\textbf{Lema 5:} Slo\v{z}enost trodimenzionalnog jednostavnog apstraktnog Voronoj dijagrama sa $n$ sedi\v{s}ta je $O(n^2)$. (Dokaz je mogu\'{c}e izvesti po indukciji za $n \geq 4$ kori\v{s}\'{c}enjem prethodnih definicija i lema)

Sada, potrebno je analizirati presek dodatog regiona sa ivicama postoje\'{c}eg dijagrama.

\textbf{Definicija 2:} Neka je \textit{e} ivica iz $V(R)$. Tada:
\begin{enumerate}
	\item Ka\v{z}emo da $u$ se\v{c}e $e$ u odnosu na $R$ ako $e \cap zatvorenje(VR(u, R \cup \{u\})) \neq \emptyset$
	\item Neka je $v$ krajnja ta\v{c}ka na $e$. Ka\v{z}emo da $u$ se\v{c}e $e$ u ta\v{c}ki $v$ u odnosu na $R$ ako $e \cap zatvorenje(VR(u, R \cup \{u\}))$ sadr\v{z}i povezanu komponentu povezanu sa $v$.
\end{enumerate}

\textbf{Definicija 3:} Neka su $p$, $q$, $r$ i $s$ razli\v{c}ita sedi\v{s}ta u $R$. Teme dijagrama $x$ nazivamo $pqrs$-teme, ako je susedno sa ova 4 regiona i $x$ je po\v{c}etna ta\v{c}ka po pravilu desne ruke. Ivica $e$ iz $V(R)$ koja je definisana regionima $p$, $q$ i $r$ iz $R$ se naziva $pqrst$-ivica, za neka sedi\v{s}ta $s, t \in R$, ako su krajevi ivice $pqrs$ i $prqt$ temena.

\textbf{Lema 6:} Neka su $p$, $q$, $r$ i $s$ razli\v{c}ita sedi\v{s}ta iz $R$. Tada $V(R)$ sadr\v{z}i najvi\v{s}e jedno $pqrs$-teme i najvi\v{s}e jednu ivicu susednu $p$, $q$ i $r$ regionu i tom temenu.

\textbf{Lema 7:} Neka je $e$ $pqrst$-ivica iz $V(R)$. Tada, za svako $R' \subset R$ sa $\{p,q,r,s,t\} \subset R'$, skup ta\v{c}aka $e$ je $pqrst$-ivica i u $V(R')$. Tako\dj{}e, za svako $u \in S \backslash R$, va\v{z}i\\$e \cap zatvorenje(VR(u, R \cup \{u\})) \neq e \cap zatvorenje(VR(u, R' \cup \{u\}))$.

\textbf{Sada mo\v{z}emo definisati osnovnu operaciju algoritma}:\\
\textit{Ulaz:} \v{S}estorka sedi\v{s}ta ($p, q, r, s, t, u$) tako da $V(\{p, q, r, s, t\}$ sadr\v{z}i $pqrst$-ivicu $e$ i $u \notin \{p, q, r, s, t\}$\\
\textit{Izlaz:} Struktura $e \cap zatvorenje(VR(u, \{p, q, r, s, t, u\}))$, koja mo\v{z}e imati neki od narednih oblika:
\begin{enumerate}
	\item Prazan skup;
	\item Presek je neprazan i sadr\v{z} jednu komponentu:
	\begin{itemize}
		\item Cela ivica $e$ ($pqrst$-ivica)
		\item Deo ivice $e$ koji je povezan na $pqrs$-kraj;
		\item Deo ivice $e$ koji je povezan na $prqt$-kraj.
	\end{itemize}
\end{enumerate}

Ukoliko je $e$ $pqrst$-ivica, onda torku $pqrst$ nazivamo opis $e$, u oznaci $\mathcal{D}(e)$. U algoritmu su nam neophodne slede\'{c}e strukture:
\begin{itemize}
	\item  Voronoj dijagram $V(R)$. Njega \v{c}uvamo u obliku grafa susedstva.
	\item Istorijski graf $\mathcal{H}(R)$. Predstavlja se u vidu stabla u kom se pamte temena i svi opisi ivica koje se u nekom trenutku pojave u algoritmu.
\end{itemize}

U istorijskom grafu je neophodno odr\v{z}avati slede\'{c}e invarijante:
\begin{enumerate}
	\item Svaki \v{c}vor ima izlazni stepen najvi\v{s}e 4. \v{C}vor koji predstavlja ivicu ima stepen 0 i on je list.
	\item Svaka ivica $e$ je povezana sa odgovaraju\'{c}im opisom $\mathcal{D}(e)$.
	\item Za svako sedi\v{s}te $u \in S \backslash R$ i svaki list $\mathcal{D}(e)$ iz $\mathcal{H}(R)$ koji se se\v{c}e sa $u$ postoji put kroz ovo stablo koje pose\'{c}uje samo \v{c}vorove koji se seku sa $u$.
\end{enumerate}

Ozna\v{c}imo sa $E_u$ skup grana iz $V(R)$ koje su prese\v{c}ene sa $u$. Defini\v{s}imo sada:
\begin{itemize}
	\item $V_{obrisano} := \{x | x \in \textit{\v{C}vor}(R)$ i sve ivice susedne sa $v$ su povezane preko $u\}$
	\item $V_{nepromenjeno} := \{x | x \in \textit{\v{C}vor}(R)$ i nijedna ivica susedna sa $x$ nije povezana preko $u\}$
	\item $V_{novo} := \{x | x \notin \textit{\v{C}vor}(R)$ i $x$ je krajnja ta\v{c}ka $e \backslash zatvorenje(\mathcal{U})$ za neko $e \in E_u\}$
\end{itemize}

\textbf{Lema 8:} $\textit{\v{C}vor}(R \cup \{u\}) = V_{nepromenjeno} \cup V_{novo}$

\textbf{Lema 9:} Za dato $E_u$, $V(R \cup \{u\})$ mo\v{z}e biti konstruisan iz $V(R)$ u vremenu $O(|E_u|)$.

\textbf{Lema 10:} Za dato $E_u$, $\mathcal{H}(R \cup \{u\})$ se mo\v{z}e konstruisati iz $V(R)$ i $\mathcal{H}(R)$ u vremenu $O(|E_u|)$.

Iz svega gore navedenog mo\v{z}emo izvesti slede\'{c}u teoremu:

\paragraph{Jednostavan apstraktni Voronoj dijagram $V(S)$ sa $n$ temena u prostoru se mo\v{z}e konstruisati pomo\'{c}u nasumi\v{c}nog algoritma u o\v{c}ekivanom vremenu i prostoru $O(n^2)$. Tako\dj{}e, $r$-to sedi\v{s}te se mo\v{z}e dodati u vremenu $O(r)$.}

\section{Primene}

Ovaj algoritam se mo\v{z}e primeniti na neke specifi\v{c}ne slu\v{c}ajeve apstrakatnih Voronoj dijagrama, kao \v{s}to su \textit{Laguerre-Voronoi} dijagrami (krugovi oko sedi\v{s}ta, svaki krug konstrui\v{s}e region), dijagrami po \textit{Hausdorff} rastojanju (najve\'{c}a vrednost rastojanja do najbli\v{z}e ta\v{c}ke koja pripada drugom skupu) i dijagrami po funkciji elipsoidnog konveksnog rastojanja.

\section{Zaklju\v{c}ak}

Iako je rezultat ovog rada doneo veliki pomak, pokazao da se mo\v{z}e inkrementalnom metodom dosta efikasno konstruisati apstraktni Voronoj dijagram za neke specifi\v{c}ne tipove ovog dijagrama u prostoru, ostaje pitanje da li se ovakav metod mo\v{z}e primeniti u op\v{s}tem slu\v{c}aju, kao i u nekim degenerisanim slu\v{c}ajevima Voronoj dijagrama. Sam rad predstavlja primenu inicijalnih ideja za konstrukciju apstraktnih Voronoj dijagrama u ravni na prostor i u mnogome se oslanja na dugogodi\v{s}nji rad Klajna\cite{conandabs}\cite{randincconstr2}.

\nocite{*}
\appendix
\addcontentsline{toc}{section}{Literatura}
\bibliography{literatura}
\bibliographystyle{plain}

\end{document}