% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}
\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}

\usepackage[english,serbian]{babel}

\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}

\usepackage{amsthm}
\usepackage{verbatim}
\usepackage{amsmath}
\allowdisplaybreaks
\newtheorem{theorem}{Teorema}[section]
\newtheorem{lemma}[theorem]{Lema}

\begin{document}

\title{Aproksimacija pravougaonih i konveksnih kontejnera minimalne površine za pakovanje konveksnih poligona\\ \small{Seminarski rad u okviru kursa\\Geometrijski algoritmi\\ Matematički fakultet}}

\author{Miloš Samardžija\\mi13304@alas.matf.bg.ac.rs}

\maketitle

\abstract {Ovaj tekst predstavlja sažetak rada \textit{Approximating minimum-area rectangular and convex containers for packing convex polygons} \cite{minareacontainers}. Autori su Helmut Alt (Institute of Computer Science, Freie Universität Berlin, Germany), Mark de Berg (Department of Computing Science, TU Eindhoven, the Netherlands) i Christian Knauer (Universität Bayreuth, Institut für Angewandte Informatik, Germany). Rad je objavljen 2017. godine u časopisu Journal of Computational Geometry.}
\tableofcontents

\newpage

\section{Uvod}
\label{sec:uvod}
Razmatrani problem ima različite varijante: najosnovniji je problem odlučivanja - da li skup datih objekata može da se smesti u zadati kontejner; problem pakovanja na traku (\textit{eng. strip packing}) - dati skup objekata mora biti spakovan na traku fiksne širine koristeći što kraće parče trake; i problem koji je razmatran u ovom radu - minimizovanje površine kontejnera u koji se pakuju dati objekti. Već najosnovnije varijante ovog problema su NP-teške. Prilikom konstrukcije aproksimativnih algoritama za problem pakovanja, od velikog značaja je oblik objekata koji se pakuju - proizvoljni pravougaonici, pravougaonici čije stranice su paralelne osama, prosti poligoni ili konveksni poligoni. Takođe, bitne su i transformacije koje mogu biti primenjene nad objektima prilikom pakovanja, odnosno, da li je objekte dozvoljeno rotirati, ili su dozvoljene samo translacije. Kontejneri u koje se pakuju objekti mogu biti konveksni poligoni ili pravougaonici čije su stranice paralelne osama.

Za objekte koji su paralelni osama i jedina dozvoljena transformacija je translacija, razvijeni su aproksimativni algoritmi sa faktorom $2$, a za objekte koji su konveksni poligoni i dozvoljene operacije su translacija i rotacija razvijeni su aproksimativni algoritmi sa faktorom $4$. U slučajevima kada je dozvoljena samo translacija, svi poznati algoritmi koji ili proizvode optimalne rezultate ili imaju dokazane aproksimativne faktore pripadaju jednoj od sledeće dve kategorije: oni pakuju samo specijalne tipove objekata poput duži i pravougaonika paralelnim osama, ili pakuju samo konstantan broj poligona. Značaj ovog rada je u tome što je pronađen algoritam složenosti $O(nlogn)$ za pakovanje $n$ konveksnih poligona u pravougaone ili konveksne kontejnere, sa dokazanim aproksimativnim faktorom. Iako je dobijeni aproksimativni faktor relativno veliki, rezultat rada je i dalje značajan s obzirom da je ovo dokaz da se NP-teški optimizacioni problemi ovog tipa uopšte mogu aproksimirati.

Algoritmi za efikasno pakovanje objekata u kontejnere imaju važne primene: u dve dimenzije - problem može biti raspoređivanje različitih obrazaca na nekom materijalu, tako da se prilikom njihovog isecanja minimizuje otpadni materijal, a u tri dimenzije - raspoređivanje objekata tako da se minimizuje prostor za njihovo skladištenje.

\section{Pravougaoni kontejneri}
\label{sec:pravougkont}
Dat je skup konveksnih poligona $P=\{p_1, ..., p_k\}$, sa ukupno $n$ temena. Potrebno je pronaći kontejner sa stranicama paralelnim osama u koji možemo spakovati poligone iz $P$, tako da površina kontejnera bude minimalna. Ukoliko je $OPT$ optimalna površina kontejnera, aproksimativni algoritam će pronaći kontejner sa površinom ne većom od $17.45*OPT$.

Visinu poligona $p$ definišemo kao razliku najveće i najmanje $y$ koordinate, i neka je $h_{max}=max_{p\in{P}}height(p)$. Na sličan način definišemo širinu poligona, kao i $w_{max}$. Ideja je da skup P particionišemo u visinske klase koristeći parametar $\alpha$ ($0<\alpha<1$) na sledeći način: poligon $p$ pripada klasi $P_i$ ako je $h_{i+1}<height(p)\leq{h_i}$, gde je $h_i=\alpha^i*h_{max}$. Nakon ovoga, algoritam se sastoji iz dva glavna koraka:
\begin{enumerate}
    \item Svaku klasu $P_i$ spakovati u odgovarajući kontejner $B_i$ visine $h_i$.
    \item Svaki neprazni kontejner $B_i$ zameniti kolekcijom mini-kontejnera paralelnim osama. Spakovati sve mini-kontejnere u rezultujući kontejner $B$.
\end{enumerate}

\subsection{I korak}
U nastavku se razmatra pakovanje klase $P_i$ u kojoj su poligoni sa visinama u opsegu $(\alpha*h_i,h_i]$. Neka je $\sigma$ polubeskonačna traka visine $h_i$. Poligone iz $P_i$ ubacujemo u $\sigma$ na sledeći način: Za poligon $p$ definišimo kičmu poligona $s(p)$ kao duž koja povezuje najnižu i najvišu tačku poligona $p$. Ukoliko $p$ ima horizontalne ivice, onda $s(p)$ povezuje donje-levo i gornje-desno teme. Sortiramo poligone iz $P_i$ na osnovu nagiba njihovih kičmi, i tim redosledom ih smeštamo u $\sigma$, gde se svaki poligon pomera udesno što je više moguće, tj. dok ne udari u levu ivicu trake $\sigma$, ili dok ne udari u neki drugi poligon. Teme sa najnižom $y$ koordinatom treba da dodiruje donju ivicu trake. Nakon smeštanja svih poligona dobijamo kontejner $B_i$ čija je desna ivica definisana krajnjim desnim temenom. Primer kontejnera $B_i$ dat je na slici \ref{fig:bi}. Lema 2.1 procenjuje gornju granicu za površinu kontejnera $B_i$.

\begin{lemma}
Površina kontejnera $B_i$ zadovoljava sledeću nejednakost:$$area(B_i)\leq 2/\alpha*\sum_{p\in P_i}area(p)+2*h_i*max_{p\in P_i}width(p)$$
\end{lemma}

\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.75]{imgs/bi_kontejner.png}
\end{center}
\caption{Pakovanje klase $P_i$ u kontejner $B_i$}
\label{fig:bi}
\end{figure}

\subsection{II korak}
Rezultat prvog koraka je niz kontejnera $B_i$ različitih dužina $l_i$, i svaki od njih sadrži poligone iz $P_i$. Svaki od kontejnera $B_i$ zamenjujemo nizom manjih kontejnera jednakih dužina. Prvo, particionišemo $B_i$ u mini-kontejnere $b$ širine $c*w_{max}$ (optimalna vrednost za $c$ će biti određena kasnije), i visine $h_i$ (osim poslednjeg pravougaonika koji može imati manju širinu). Nakon toga se svaki poligon $p\in P_i$ pridružuje kontejneru $b$ koji sadrži njegovo krajnje levo teme. Od svakog kontejnera $b$ se generiše novi kontejner koji nastaje proširivanjem kontejnera $b$ do širine $(c+1)*w_{max}$. Tako produžen kontejner $b$ sadrži sve poligone pridružene prvobitnom kontejneru $b$. Rezultat ovoga je kolekcija $\overline{R_i}$ od najviše $l_i/(c*w_{max})+1$ mini-kontejnera čije su širine tačno $(c+1)*w_{max}$. Pošto su visine $B_i$ i svakog mini-kontejnera jednake i iznose $h_i$, važi nejednakost $$\sum_{b\in\overline{R_i}}area(b)\leq (1+\frac{1}{c})*area(B_i)+(c+1)*w_{max}*h_i$$

Neka je $\overline{R}=\bigcup\overline{R_i}$ kolekcija svih mini-kontejnera dobijenih na ovaj način. Pakovanje ovakvih kontejnera u jedan veliki se može izvršiti trivijalno, s obzirom da svi imaju istu širinu: sve kontejnere iz $\overline{R}$ slažemo jedan na drugi, i na taj način dobijamo rezultujući kontejner $B$. U nastavku sledi teorema o gornjoj granici faktora aproksimacije ovog algoritma.
\begin{theorem}
Neka je $P$ skup poligona u ravni sa ukupno $n$ temena. U vremenskoj složenosti $O(nlogn)$ možemo izvršiti pakovanje skupa $P$ u pravougaoni kontejner $B$ za koji važi $area(B)\leq 17.45*OPT$.
\end{theorem}
\begin{proof}
Primetimo da važi $OPT\geq \sum_{p\in P}area(p)$ i $OPT\geq w_{max}*h_{max}$.
\begin{align*}
area(B) &= \sum_{b\in\overline{R}}area(b)\\
& \leq \sum_i\left\{(1+\frac{1}{c})*area(B_i)+(c+1)*w_{max}*h_i\right\}\\
& \leq (1+\frac{1}{c})\sum_i\left\{2/\alpha*\sum_{P\in P_i}area(p)+2*h_i*max_{p\in P_i}*width(p)\right\}\\
& \quad+\frac{1}{1-\alpha}*(c+1)*w_{max}*h_{max}\\
& \leq (1+\frac{1}{c})*(2/\alpha*\sum_{p\in P}area(p)+2/(1-\alpha)*w_{max}*h_{max})\\
& \quad+\frac{1}{1-\alpha}*(c+1)*OPT\\
& \leq \left((1+\frac{1}{c})*(\frac{2}{\alpha}+\frac{2}{1-\alpha})+\frac{c+1}{1-\alpha}\right)*OPT
\end{align*}
Faktor ispred $OPT$ se uprošćava u $f(c, \alpha)=(1*\frac{1}{c})*\frac{2+c*\alpha}{\alpha-\alpha^2}$.
Da bismo pronašli optimalne $\alpha$ i $c$, izračunaćemo parcijalne izvode i videćemo kad su oni nula. Rešavanjem jednačina dobijamo da su $c=2.214..$ i $\alpha=0.407..$, pa aproksimacioni faktor iznosi $17.449..$.

Da bismo dobili željenu vremensku složenost, prvo je potrebno primetiti da se particionisanje poligona u visinske klase može obaviti u linearnom vremenu, a nakon toga sortiranje na osnovu nagiba kičme u $O(nlogn)$. Da bismo u koraku I efikasno ubacivali poligone u $B_i$, održavaćemo binarno balansirano stablo pretrage $T$, u kojem je uređenje prema vrednostima $y$ koordinata. Stablo sadrži $E$ - skup temena iz $B_i$ vidljivih sa desne strane. Da bismo pronašli tačku u kojoj se sudaraju ubačeni poligoni i poligon $p$ koji se ubacuje, za svako teme iz $p$ koje je vidljivo sa leve strane se u $T$ pronalaze tačke koje obrazuju stranicu u koju teme može potencijalno da udari. Ovo zahteva $O(logn)$ za svako teme iz $p$ koje je vidljivo sa leve strane. Takođe, potrebno je uraditi i obrnuto, odnosno, za svako teme iz $B_i$ vidljivo sa desne strane potrebno je pronaći potencijalnu stranicu iz $p$ u koju može da udari. Ovo takođe zahteva $O(logn)$ po temenu, ako pretpostavimo da su temena u $p$ sortirana u smeru kazaljke na satu. Nakon toga se računa rastojanje između svakog temena i odgovarajuće stranice kandidata, i par sa najkraćim rastojanjem određuje tačku sudaranja poligona $p$ sa $B_i$. Posle ubacivanja $p$ u $B_i$, potrebno je ažurirati temena vidljiva sa desne strane tako što se iz stabla izbacuju temena koja zaklanja poligon $p$, a ubacuju se temena poligona $p$ vidljiva sa desne strane. Pošto se svako teme ubacuje i izbacuje iz stabla najviše jednom, ovo zahteva $O(nlogn)$ koraka, pa je celokupna složenost prvog koraka $O(nlogn)$. Iskorišćena struktura podataka je prikazana na slici \ref{fig:sp}.
\end{proof}

\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.75]{imgs/sp.png}
\end{center}
\caption{Struktura podataka za ubacivanje poligona u $B_i$}
\label{fig:sp}
\end{figure}

\section{Konveksni kontejneri}
Ovaj problem se razlikuje od prethodnog u tome što je potrebno pronaći konveksni kontejner. Drugim rečima, potrebno je spakovati skup konveksnih poligona tako da površina konveksnog omotača njihove unije bude minimalna.

Ideja je da se pronađe odgovarajuća orijentacija $\theta$ (poligona iz $P$) koja minimizuje izraz $h_{max}(\theta)*w_{max}(\theta)$, a nakon toga se nad tim primeni prethodni algoritam za pronalaženje pravougaonog kontejnera, i dobijeni kontejner $B$ se vrati kao aproksimirano rešenje. U nastavku sledi teorema o gornjoj granici faktora aproksimacije ovog algoritma. Dokaz se može izvesti na sličan način kao za prethodnu teoremu.
\begin{theorem}
Neka je $P$ skup konveksnih poligona u ravni sa ukupno $n$ temena. U vremenskoj složenosti $O(nlogn)$ možemo izvršiti pakovanje skupa $P$ u konveksni poligon $B$ za koji važi $area(B)\leq 27*OPT$.
\end{theorem}

\addcontentsline{toc}{section}{Literatura}
\appendix
\bibliography{seminarski} 
\bibliographystyle{plain}
\end{document}
