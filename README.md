## Prikaz naucnog rada

Materijal moze biti u vidu: 

 - �lanka (3-4 strane, LaTeX) 
ILI
 - slajdova (10-15, LaTeX - beamer) 

Po zavr�etku rada, izvorni materijal plus PDF poslati mejlom profesoru, sa asistentkinjom u cc.

Radovi ce biti ocenjeni nakon odbrane. 

Jedan od ciljeva ovog projekta da u izabranom radu uo�ite �ta je najbitnite i da to prika�ete i istaknete. 

U okviru materijala treba obraditi slede�a pitanja: 

- Ko su (i odakle su) autori rada 
- Gde i kada je objavljen rad 
- Najpre kratak, neformalan opis problema i za�to je bitan 
- Opis glavnih rezultata (teoreme/dokazi, algoritmi, implementacije, ..) 
- Opis primena 
- Kako se opisani rezultati odnose na druge relevantne rezultate (nijedan rad nije nezavisan od svega ostalog; dakle - da li donosi uop�tenje tvrdjenja, unapredjeni algoritam, nove primene, itd) 

Dr�ati se preporu�enog obima materijala, prekora�enja u bilo kom smeru smatraju se propustom. 
