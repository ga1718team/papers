% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}
\usepackage[T2A]{fontenc} % enable Cyrillic fonts
\usepackage[utf8]{inputenc} % make weird characters work
\usepackage{graphicx}
\usepackage[english,serbian]{babel}
\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}

\newtheorem{lema}{Lema}[section]

\begin{document}

\title{Granice za stepenove u pseudo-triangulaciji tačaka\\ \small{Prikaz naučnog rada u okviru kursa\\Geometrijski algoritmi\\ Matematički fakultet}}

\author{Milica Đurić\\ mdjuric55@gmail.com}
\date{31.~decembar 2017.}
\maketitle

\abstract{
Ovaj rad predstavlja prikaz razumevanja naučnog rada koji se nalazi na adresi \url{https://www.sciencedirect.com/science/article/pii/S0925772102001268}.  Prikazano je da svaki skup tačaka u opštem položaju ima minimalnu pseudo-triangulaciju sa maksimalnim stepenom čvora pet i minimalnu pseudo-triangulaciju u kojoj svaka unutrašnja oblast ima najviše četiri čvora. Prikazani su i algoritmi vremenske složenosti $O(nlogn)$ za njihovo konstruisanje.
}

\section{Uvod}
\label{sec:uvod}


Zadatak ovog rada jeste prikazati naučni rad na temu ''Tight degree bounds for pseudo-triangulations of points''. Autori ovog naučnog rada jesu Lutz Kettner iz Nemačke, David Kirkpatrick iz Kanade, Andrea Mantler i Jack Snoeyink iz Amerike, Bettina Speckmann iz Švajcarske i Fumihiko Takeuchi iz Japana. Rad je objavljen u časopisu Computational Geometry maja 2003. godine, u izdanju 25, broj 1-2.

Pomenuti rad bavi se formiranjem pseudo-triangulacija i to ne bilo kakvih, nego minimalnih pseudo-triangulacija koje imaju nekakva ograničenja. Treba pronaći koji je minimalan maksimalan stepen čvora koji  svaka minimalna pseudo-triangulacija tačaka ima i definisati algoritam koji formira takvu triangulaciju za proizvoljan skup tačaka. Drugi problem je definisanje algoritma koji formira minimalnu pseudo-trianuglaciju koja ima ograničen broj čvorova koji mogu da se nađu u jednom pseudo-trouglu u triangulaciji. Problem nalaženje minimalne pseudo-triangulacije je jako bitan u efikasnoj detekciji kolizije kod pomerajućih poligona u ravni.


\section{Osnovni pojmovi i primene}

\textit{\textbf{Pseudo-trougao}} jeste planaran poligon koji ima \textit{tačno} tri konveksna čvora, koji se nazivaju \textbf{ćoškovi}, u kojima je unutrašnji ugao manji od $\pi$, a u ostalim čvorovima je ugao veći od $\pi$. Skup ivica koje spajaju dva ćoška se naziva \textbf{strana}.

\textit{\textbf{Pseudo-triangulacija} } skupa $S$ od $n$ tačaka jeste podela konveksong omotača od $S$ na pseudo-trouglove čiji su svi čvorovi iz $S$.

Neke od pseudo-triangulacija prikazane su na sledećoj slici.
\begin{center}
\includegraphics[scale=0.4]{pseudo}
\end{center}

\textit{\textbf{Minimalna pseudo-triangulacija}} datih tačaka jeste ona pseudo-triangulacija koja ima najmanji broj ivica, ili, ekvivalentno, najmanji broj oblasti. Svaka ovakva triangulacija ima $n-2$ pseudo-trougla i $2n-3$ ivica, gde je $n$ broj tačaka.

\begin{lema}
Pseudo-triangulacija skupa tačaka $S$ jeste minimalna akko svaka tačka $p$ jeste deo pseudo-trougla ili spoljašnje oblasti u kojem je ugao kod $p$ veći od $\pi$.
\end{lema}


Pseudo-triangulacije nalaze svoje primene u problemu vidljivosti, kao i u problemima planiranja kretanja robota. Kao najvažnija primena, ističe se već pomenuta detekcija kolizije. Slobodan prostor između pomerajućih poligona u ravni se pseudo-trianguliše i takav prostor se lako održava korišćenjem zamena ivica prilikom pomeranja poligona.


\section{Algoritmi konstruisanja pseudo-triangulacija}
\label{sec:naslov1}
U ovoj sekciji biće opisani prikazani algoritmi za dobijanje minimalnih pseudo-triangulacija u kojima je ograničen stepen čvora na 5 ili stepen oblasti na 4. Zajednički algoritam, do ovog momenta, još nije smišljen. Prvi algoritam je rekurzivan, a drugi iterativan.

\subsection{Ograničen stepen čvora}
\label{subsec:podnaslov1}
Pre definisanja samog algoritma potrebno je uvesti oznake $B(P)$ i $s(P)$ - tačke koje se nalaze na obodu konveksnog omotača $P$ datih tačaka  i njihov broj. Opterećenje čvora u oznaci $l(p)$ jeste stepen čvora minus dva, dok $l(P)$ označava zbir opterećenja svih čvorova iz $B(P)$.


Algoritam se sastoji iz dve osnovne operacije - \textbf{particionisanje} i \textbf{orezivanje} (\textit{eng. prune}). Ukoliko sve tačke u okviru $B(P)$ imaju $l(p)$ jedan, onda se izvršava particionisanje, u suprotnom, ako neka od tačaka iz $B(P)$ ima $l(p)$ dva ili tri, izvršava se orezivanje. Obe ove operacije se izvršavaju s namerom da se dobiju poligoni sa manjim brojem tačaka.

Tokom čitavog izvršavanja algoritma važe dve invarijante:
\begin{enumerate}
\item Za svaki $p$ iz $B(P)$ važi $l(p) \leq 3$. Jednakost važi za najviše jedan $p$.
\item Ako je $l(p) \leq 2$ za svaki $p$ iz $B(P)$, onda je $l(P)\leq 5$. Inače, $l(P)\leq 6$.
\end{enumerate}

Da je dobijena pseudo-triangulacija baš minimalna, može se dokazati korišćenjem prve invarijante i Leme 1.1.


Korišćenjem binarnih stabala koji prikazuje gornji omotač i donji omotač i koji čuvaju u svakom čvoru odgovarajuće tangente, složenost ovog algoritma jeste $O(nlogn)$.
\subsubsection{Particionisanje}

Razlikujemo dva slučaja, prvi je kada zadati poligon ima $l(P)= 5$, a drugi je kada je $l(P) < 5$. U prvom slučaju iz skupa tačaka koje imaju $l(p)= 1$ biramo onu tačku koja je središnja po x koordinati, a u drugom slučaju iz skupa svih tačaka biramo tačku koja nema ekstremnu x vrednost i za koju važi da se najviše dve tačke sa $l(p)= 1$ nalaze sa jedne strane vertikalne prave koja prolazi kroz tu tačku. Sledeći korak je formirati dva konveksna poligona od skupa tačaka koje se nalaze sa leve i desne strane vertikalne linije koja prolazi kroz izabranu tačku. S obzirom da oba poligona sadrže izabranu tačku, njeno opterećenje je za dva veće, odnosno može biti ili 2 ili 3, odakle sledi da je prva invarijanta zadovoljena. S obzirom da sa jedne strane vertikalne prave može da postoji najviše dve tačke sa opterećenjem jedan, a da formiranje konveksnog omotača od podskupa sa jedne strane može da donese najviše povećanju opterećenja za jedan, onda je zadovoljena i druga invarijanta.
\subsubsection{Orezivanje}

U ovoj operaciji izdvajamo jedan čvor $p$ koji pripada $B(P)$ i za koji važi da ima najveći $l(P)$. Zatim se formira konveksan poligon $P'$ od ostatka tačaka. I primenom ove operacije zadržavaju se važenja invarijanti. Druga invarijanta važi s obzirom da je $l(P') = l(P) - l(p)+2$, pa zamenom $l(p)$ sa 2 ili 3 dobijamo da je $l(P') \leq 5 $. Prva invarijanta važi na osnovu sledećeg: na osnovu druge invarijante, u ostatku $B(P)$ mora da postoji najviše jedan čvor sa opterećenjem 2, i on može da bude susedan čvoru $p$. Pravljenjem $P'$ može se samo od tog čvora napraviti čvor sa opterećenjem 3. Ovo važi i za slučaj kada je $l(P') = 2$ i $l(P') = 3$.

\begin{figure}
\begin{center}
\includegraphics[scale=0.4]{partitionAndPrune}
\end{center}
\caption{Particionisanje i orezivanje}
\end{figure}



\subsubsection{Skup tačaka koje zahtevaju stepen čvora 5}

U ovoj sekciji biće dokazano da je granica za stepen čvora 5 najmanja moguća i da bilo koji pravilan jedanaestougao sa centralnom tačkom mora da ima čvor stepena pet. 
\begin{center}
\includegraphics[scale=0.5]{12points}
\end{center}
Svi ostali unutrašnji pseudo-trouglovi biće trouglovi jer nijedan drugi ugao nije veći od $\pi$. Bilo koja linija koja prolazi kroz centralnu tačku ima sa jedne svoje strane barem 5 tačaka, što znači da nijedna ivica unutrašnjeg pseudo-trougla ne pripada ivicama konveksnog omotača, što znači da ćoškovi unutrašnjeg pseudo-trougla imaju stepen čvora 4. Da bi se triangulacija završila do kraja, potrebno je da barem jedna ivica nekog od novonastalih pseudo-trouglova ima za svoj kraj neki od ovih ćoškova. To znači da barem jedan čvor mora da ima stepen 5. 
\subsection{Ograničen stepen oblasti}
Hoćemo da pokažemo da postoji minimalna pseudo-triangulacija bilo kojeg skupa tačaka za koju važi da svaki pseudo-trougao ima najviše 4 čvora.

Prvi korak algoritma treba da napravi minimalnu pseudo-triangulaciju konveksnog omotača skupa tačaka. Sledeći koraci su ubacivanje unutrašnjih tačaka i formiranje pseudo-trouglova čiji je jedan čvor novoubačena tačka. Kada se ubaci nova tačka, potrebno je pronaći kom pseudo-trouglu u trenutnoj pseudo-triangulaciji ona pripada. Ukoliko je pronađeni pseudo-trougao zapravo trougao, sve što treba uraditi je spojiti bilo koja dva njegova ćoška sa novom tačkom. Međutim, ukoliko pronađeni pseudo-trougao ima 4 čvora, onda produžavajući njegove dve stranice koje formiraju jednu stranu, delimo njegovu unutršanjost na tri dela. U zavisnosti od toga u kom delu se nova tačka nalazi, spajamo određena dva čvora sa njom. Ovo je prikazano na sledećoj slici.
\begin{center}
\includegraphics[scale=0.5]{insertPoint}
\end{center}
Sortiranje tačaka, stvaranje početne pseudo-triangulacije i građenje strukture za lociranje tačke zahteva $O(nlogn)$ vremena. Ukoliko nove tačke ubacujemo po rastućoj x-koordinati, onda se i naredni koraci mogu izvršiti za  $O(nlogn)$ vremena.



\section{Zaključak}
\label{sec:zakljucak}
Ovaj rad je bitan jer je poznato da postoje skupovi $n$ tačaka za koje važi da bilo koja triangulacija ima čvor stepena $n-1$, dok je ovde bilo prikazano da za svaki skup tačaka postoji minimalna pseudo-triangulacija čiji je maksimalni stepen čvora pet i da je to najbolji mogući rezultat. Znači, otkriveno je da je moguće ograničiti stepen čvora kod pseudo-triangulacija, dok  kod triangulacija nije moguće. Takođe, iako pseudo-triangulacije imaju veći stepen oblasti (broj čvorova koji čine tu oblast) nego obične triangulacije, moguće je za svaki skup tačaka pronaći minimalnu pseudo-triangulaciju u kojoj se svaka oblast sastoji od tri ili četiri čvora, što znači da je moguće čak i ograničiti i stepen oblasti, a da pseudo-triangulacija svakako ostane minimalna.


Ono što bi trebalo dalje istražiti jeste da za dati skup od $n$ tačaka pronaći minimalnu pseudo-triangulaciju koja sadrži minimalni maksimalni stepen čvora.


\end{document}
